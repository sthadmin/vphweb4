﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace VphWeb4
{
    public class CodeClass
    {
        public string GetDatasetName()
        {
            // Remove protocol 
            string urlProtocolRemoved = HttpContext.Current.Request.Url.ToString().ToLower().Replace("http://", "").Replace("https://", "");
            string[] urlSplit = urlProtocolRemoved.Split('/');
            // Check that a dataset has been supplied 
            if (urlSplit.Length < 2)
            {
                return null;
            }
            // Check dataset name isn't a restricted name 
            if (urlSplit[1] == "admin" || urlSplit[1] == "dps")
            {
                return null;
            }
            return urlSplit[1];
        }



        public DataTable GetTableInfo(string connectionName, string storedProc, Dictionary<string, SqlParameter> procParameters)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(storedProc, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (var procParameter in procParameters)
                {
                    cmd.Parameters.Add(procParameter.Value);
                }

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                    return dt;
                }
            }
        }


        public int ExecuteCommand(string connectionName, string storedProc, Dictionary<string, SqlParameter> procParameters)
        {
            int rc;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(storedProc, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (var procParameter in procParameters)
                {
                    cmd.Parameters.Add(procParameter.Value);
                }

                rc = cmd.ExecuteNonQuery();
            }

            return rc;
        }



        public DataTable LoadData()
        {
            string name = GetDatasetName();

            Dictionary<string, SqlParameter> queryParameters = new Dictionary<string, SqlParameter>();
            queryParameters["@name"] = new SqlParameter("@name", name);

            DataTable t = GetTableInfo("VphShareWeb", "GetSingleTableData", queryParameters);

            return t;
        }


        public int ExecuteUpdateCommand()
        {
            string name = GetDatasetName();

            Dictionary<string, SqlParameter> queryParameters = new Dictionary<string, SqlParameter>();

            queryParameters["@name"] = new SqlParameter("@name", name);

            int c = ExecuteCommand("VphShareWeb", "UpdateData", queryParameters);

            return c;
        }
    }
}


 //void GridView1RowDataBound(object sender, GridViewRowEventArgs e)
 //       {
 //           if (e.Row.RowType == DataControlRowType.DataRow)
 //           {
 //               var firstCell = e.Row.Cells[0];
 //               firstCell.Controls.Clear();
 //               firstCell.Controls.Add(new HyperLink { NavigateUrl = firstCell.Text, Text = firstCell.Text });
                                                                    
 //           }
 //       }