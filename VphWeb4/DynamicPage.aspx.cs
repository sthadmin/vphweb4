﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace VphWeb4
{
    public partial class DynamicPage : System.Web.UI.Page
    {

        CodeClass u = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = u.LoadData();
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in u.LoadData().Rows)
            {
                if (row["Status"].ToString() == "Error")
                {
                    int d = u.ExecuteUpdateCommand();
                    GridView1.DataSource = u.LoadData();
                    GridView1.DataBind();

                    Label1.Text = (+d + " dataset(s) modified successfully");
                }
            }         
        }
    }
}