﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace VphWeb4
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            Application["Applications"] = 0;
            Application["UserSessions"] = 0;

            Application["Applications"] = (int) Application["Applications"] + 1;

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            Application["UserSessions"] = (int) Application["UserSessions"] + 1;

        }

        void Session_End(object sender, EventArgs e)
        {
            Application["UserSessions"] = (int)Application["UserSessions"] - 1;

        }

        static void RegisterRoutes(RouteCollection routes)
        {
            //routes.MapPageRoute("DynamicPage", "Page/{pageName.aspx}", "~/DynamicPage.aspx");

            routes.MapPageRoute("VphDataset", "{pvp}/{pageName.aspx}", "~/DynamicPage.aspx");
        }

    }
}
