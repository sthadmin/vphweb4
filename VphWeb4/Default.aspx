﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="VphWeb4._Default" %>
<%@ Import Namespace="VphWeb4" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to the VphShare Datasets Page!
    </h2>
    <p>
        <asp:GridView ID="GridView1" runat="server" HeaderStyle-BackColor="#7DB4D3" HeaderStyle-ForeColor="White"
    RowStyle-BackColor="#E2EBF0" AlternatingRowStyle-BackColor="White" AlternatingRowStyle-ForeColor="#000"
    runat="server">
        </asp:GridView>
        <asp:Button ID="Button1" runat="server" Text="Reset" />
    </p>
    </asp:Content>
