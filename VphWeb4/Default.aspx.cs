﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VphWeb4
{
    public partial class _Default : System.Web.UI.Page
    {
        CodeClass cc = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(! IsPostBack)
            {
                BindPages();
            }
        }

        private void BindPages()
        {
            var queryParameter = new Dictionary<string, SqlParameter>();
            DataTable t = cc.GetTableInfo("VphShareWeb", "GetAllTableData", queryParameter);
            GridView1.DataSource = t;
            GridView1.RowDataBound += GridView1RowDataBound;
            GridView1.DataBind();            
        }         


        protected void GridView1RowDataBound(object sender, GridViewRowEventArgs e)
        {
            HyperLink the_url = new HyperLink();
            //string currentUrl = HttpContext.Current.Request.Url.Host;
            var firstCell = e.Row.Cells[0];
            //the_url.NavigateUrl = firstCell.Text + @"/" + firstCell.Text + @".aspx";
            the_url.NavigateUrl = firstCell.Text + @"/DynamicPage.aspx";
            the_url.Text = firstCell.Text;

            e.Row.Cells[0].Controls.Add(the_url);
        }


    }
}
